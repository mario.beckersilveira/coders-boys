# Coders Boys

Revision 4 IS3


Para poder realizar el deploy de la aplicación se deben realizar los siguientes pasos:

- Verificar si se tiene instalado NodeJs ejecutando el comando (CMD) node -v  en caso de tener
  instalado nos va a devolver que version tenemos y en caso y en caso de no tener instalado
  pueden descargar del siguiente link: https://nodejs.org/es/
- Clonar el proyecto del Git en una carpeta local Ejemplo -> cd C:\Users\USERS\revision_4
  - git clone https://gitlab.com/mario.beckersilveira/coders-boys.git

- Primeramente ejecutamos CMD y nos posicionamos en la carpeta de nuestro proyecto
  a modo de ejemplo se adjunta el comando del proyecto de mi PC
  - C:\Users\USER\Desktop\Revision_4>cd coders-boys
  - C:\Users\USER\Desktop\Revision_4\coders-boys>npm install express

- Posteriormente se deben correr el siguiente comando para instalar dentro de nuestro proyecto.
  + npm install express

- Finalmente se puede correr el comando npm run start y la aplicacion quedará publicada
  locamente en el puerto 3002 link de acceso: http://localhost:3002/index.html
-

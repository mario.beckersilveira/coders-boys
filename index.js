import express  from 'express';
import path from 'path';
const app = express();

app.use(express.static('public'))

app.get('/index.html', (req,res) =>{
    console.log('Se recibio una solicitud a /index');
    res.sendFile(path.join(path.resolve(),'', 'index.html'));
});

app.get('/public/herramientas.html', (req,res) =>{
    res.sendFile(path.join(path.resolve(),'public','herramientas.html'));
});

app.get('/public/recurso_interesante.html', (req,res) =>{
    res.sendFile(path.join(path.resolve(),'public','recurso_interesante.html'));
});

app.get('/public/recurso_utilizado.html', (req,res) =>{
    res.sendFile(path.join(path.resolve(),'public','recurso_utilizado.html'));
});

app.get('/public/recurso_video.html', (req,res) =>{
    res.sendFile(path.join(path.resolve(),'public','recurso_video.html'));
});

app.listen(3002, () =>{
   console.log('corriendo en el puerto 3002');
});


